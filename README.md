# **R code developed for the statistical analysis of data from the DEMISEL_field project**.

[![R](https://img.shields.io/badge/R-4.3.1-23aa62.svg?labelColor=000000)](https://cran.r-project.org/)


## Introduction

You will find in this project, all the R code developed within the framework of the biostatistical analyses of the data from the DEMISEL_field project:
- statistical tests (alpha diversity analyses, LEfSe analysis, and core microbiome investigation with UpSetR analysis)
- visualization of results (creation of figures for the publication) 

The sequencing dataset was deposited in the European Nucleotide Archive (ENA) under the project number PRJEB70923. Bioinformatic analyses of the raw data were performed using SAMBA (https://gitlab.ifremer.fr/bioinfo/workflows/samba; v4.0.0), a standardized and automatized metabarcoding workflow developed by the Ifremer Bioinformatics Platform (SeBiMER). Two ASV table and PHYLOSEQ object have been created to distinguish the total bacterial community and the heterotrophic bacterial community (without Cyanobacteria genus). Statistical tests were carried out on the heterotrophic bacterial community (without Cyanobacteria genus). 

i. To download the code and the data of DEMISEL_field project

```bash
git clone https://gitlab.com/oreignie/DEMISEL_field.git
```

## Credits

R code is written by Océane Reignier.

